<?php

require_once("{$ROOT}{$DS}model{$DS}ModelPhoto.php");
?>
<style>
    <?php
    require_once("{$ROOT}{$DS}public{$DS}css{$DS}readproduit.css");

    require_once("{$ROOT}{$DS}public{$DS}css{$DS}readProduitAvis.css");
    ?>
</style>
<!-- mise en forme avec tiny.cloud -->
<script src="https://cdn.tiny.cloud/1/q6ee3shassxxvikgd3pm8sdhl53uviqw7488gj8mqz9fpk8t/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
        toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
    });
</script>
<?php
//var_dump( $_SESSION['first_name']);
//var_dump($_SESSION['test']);
if (isset($_SESSION['AV_create'])) {
    echo '<center style="margin-top: 2px"><div class=" col-7 alert alert-success" role="alert"><strong> ' . $_SESSION['AV_create'] . '</strong> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div></center>';
    unset($_SESSION['AV_create']);
}

if (isset($_SESSION['delete_avis'])) {
    echo '<center style="margin-top: 2px"><div class=" col-7 alert alert-success" role="alert"><strong> ' . $_SESSION['delete_avis'] . '</strong> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div></center>';
    unset($_SESSION['delete_avis']);
}
?>
<div class="container">


    <h1 class="my-4"><i class="fa fa-product-hunt" aria-hidden="true"
                        style="color: steelblue;"></i> <?= $u->getLabel() ?>
    </h1>
    <div class="row">

        <div class="col-md-8">


            <div id="demo" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ul class="carousel-indicators">

                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>


                </ul>
                <div class="carousel-inner">
                    <div class="carousel-item active "><img src="<?= ROOT_IMG ?>produit/A_ps5.jpg" style="width: 100%"
                                                            width="800" height="500"></div>
                    <div class="carousel-item "><img src="<?= ROOT_IMG ?>produit/PlayStation.jpg" style="width: 100%"
                                                     width="800" height="500"></div>
                    <div class="carousel-item "><img src="<?= ROOT_IMG ?>produit/resize.jpg" style="width: 100%"
                                                     width="800" height="500"></div>


                </div>


                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>

            </div>
        </div>

        <div class="col-md-4">
            <h3 class="my-3"> Description de produit </h3>
            <p><i class="fa fa-arrow-right" aria-hidden="true" style="color: #007b5e;"></i>
                <?php if ($u->getDescription() != Null) {
                    echo $u->getDescription();
                } else {
                    echo '<strong>No description</strong>';
                } ?>.</p>
            <h3 class="my-3">Details de produit </h3>
            <ul>
                <li style="margin-top: 15px;"> <span><h5>  Prix : <?= $u->getPrix() ?> <i class="fa fa-eur"
                                                                                          aria-hidden="true"
                                                                                          style="color: #007b5e;">  </i>  </h5>
                </li>
                <li style="margin-top: 15px;"><span><h5>  Quantité : <?= $u->getQuantite() ?>   </h5></li>

                <?php

                if ($listAvis != null) {
                    $x = 0;
                    $somme = 0;
                    foreach ($listAvis as $un_avi) {
                        $somme = $somme + $un_avi->getNote();
                        $x++;
                    }
                    $moyenne = $somme / $x;
                    echo '<li style="margin-top: 15px;"> <span><h5>  Moyenne des notes : ' . round($moyenne, 2) . ' / 5    </h5> </li>';
                }

                ?>


            </ul>
            <a href='#' data-toggle="modal" data-target="#Add_avi">
                <button type="button" name="walidii" class="btn btn-primary my-2 my-sm-0">Donnervotre avis</button>
            </a>
        </div>

    </div>




    <div class="row">
        <?php

        if ($listAvis != null) {
        ?>



            <div class="dropdown mt-4" style="margin-left: 15px">
                <h6 class=" dropdown-toggle" type="button" id="dropdownMenu1"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Filtrer par
                </h6>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">

                    <li class="dropdown-item"><a href="index.php?controller=produit&action=read&id=<?= $u->getId() ?>&note=1"> Note = 1</a></li>
                    <li class="dropdown-item"><a href="index.php?controller=produit&action=read&id=<?= $u->getId() ?>&note=2"> Note = 2</a></li>
                    <li class="dropdown-item"><a href="index.php?controller=produit&action=read&id=<?= $u->getId() ?>&note=3"> Note = 3</a></li>
                    <li class="dropdown-item"><a href="index.php?controller=produit&action=read&id=<?= $u->getId() ?>&note=4"> Note = 4</a></li>
                    <li class="dropdown-item"><a href="index.php?controller=produit&action=read&id=<?= $u->getId() ?>&note=5"> Note = 5</a></li>

                </ul>
            </div>



            <div class="dropdown mt-4" style="margin-left: 49%">
                <h6 class=" dropdown-toggle" type="button" id="dropdownMenu1"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Trier par
                </h6>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">

                    <li class="dropdown-submenu">
                        <a class="dropdown-item" tabindex="-1" href="#">Date</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-item"><a tabindex="-1" href="index.php?controller=produit&action=read&id=<?= $u->getId() ?>&trier=date_asc"> <span> <i class="fa fa-arrow-up" aria-hidden="true" style="color: green !important;"></i></span> Ascendant</a></li>
                            <li class="dropdown-item"><a tabindex="-1" href="index.php?controller=produit&action=read&id=<?= $u->getId() ?>&trier=date_desc"> <span> <i class="fa fa-arrow-down" aria-hidden="true" style="color: red !important;"></i></span> Descendant </a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a class="dropdown-item" tabindex="-1" href="#">Note</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-item"><a tabindex="-1" href="index.php?controller=produit&action=read&id=<?= $u->getId() ?>&trier=note_asc"><span> <i class="fa fa-arrow-up" aria-hidden="true" style="color: green !important;"></i></span> Ascendant</a></li>
                            <li class="dropdown-item"><a tabindex="-1" href="index.php?controller=produit&action=read&id=<?= $u->getId() ?>&trier=note_desc"> <span> <i class="fa fa-arrow-down" aria-hidden="true" style="color: red !important;"></i></span> Descendant</a></li>
                        </ul>
                    </li>

                </ul>
            </div>

        <?php
        }
        ?>


        <?php

        if ($listAvis != null) {
            foreach ($listAvis as $un_avi) {
                $img = ModelPhoto::selectPhotoByAvi($un_avi->getId());

                ?>
                <div class="jumbotron col-md-8">
                    <div class="avis">
                        <div class="s_avis">
                            <div class="img">
                                <img height="48" width="48" src="<?= ROOT_IMG ?>avi/<?php if ($img != Null) {
                                    echo $img->getName() . '.' . $img->getExtension();
                                } else {
                                    echo 'default.png';
                                } ?>"/>
                            </div>
                            <div class="avis_contenu ">
                                <p class="auteur">Publié par <?php echo $un_avi->getPseudo();
                                    if ($un_avi->getNote() > 0) {
                                        echo "<span style=\"position: center; margin-left: 10px;\">  ";
                                        for ($n = 0; $n < $un_avi->getNote(); $n++) {
                                            echo " <i class=\"fa fa-star\" aria-hidden=\"true\"
                                                                               style=\"color: gold\"></i>";
                                        }
                                        echo " </span></p>";
                                    } ?>
                                <p class="avis_date">
                                    Le <?php echo str_replace(" ", " à ", str_replace("-", "/", $un_avi->getCreatedAt())); ?>
                                    <a href="index.php?controller=avi&action=delete&id=<?= $un_avi->getId() ?>&produit=<?= $u->getId() ?>"
                                       onclick="return confirm('Êtes-vous sûr de vouloir SUPPRIMER cet avi ?');">
                                        Supprimer</a></p>
                                <p class="what"> <?= $un_avi->getCommentaire() ?></p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php


            }


        } else {
            echo '<strong style="margin-top: 20px">Liste des avis vide   </strong>';
        }


        ?>

    </div class="row">
    <!-- ajouter un avis avec une formulaire simple -->
    <div style="border: #39D2B4">
        <form action="index.php?controller=avi&action=create&produit=<?php echo $u->getId(); ?>"
              enctype="multipart/form-data" method="post" style="margin-top: 1%; border: #39D2B4">
            <legend>Donner votre avis</legend>
            <div class="form-row ">
                <div class="form-group  col-md-4">
                    <label for="exampleFormControlInput1">Email</label>
                    <input type="email" name="email" class="form-control" id="inputEmail4" placeholder="Your Email"
                           required>
                </div>
                <div class="form-group col-md-2">
                    <label for="exampleFormControlInput1">Pseudo</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">@</div>
                        </div>
                        <input type="text" name="pseudo" class="form-control" id="inlineFormInputGroup"
                               placeholder="Username" required>
                    </div>
                </div>
                <div class="form-group  col-md-1">
                    <label for="exampleFormControlInput1">Note</label>
                    <select class="form-control form-control " name="note">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>

                    </select>
                </div>
                <div class="form-group  col-md-2">
                    <label>Votre Photo</label>
                    <div class="file">

                        <label for="file">Ajouter</label>
                        <input type="file" name="photo" id="file" style="margin-bottom: 12px">

                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-8">
                    <label for="exampleFormControlInput1">Commentaire</label><span style="color: red;"> *</span>
                    <textarea class="form-control" name="commentaire" id="exampleFormControlTextarea1"
                              placeholder="Saisi votre commentaire" rows="3" maxlength="3000"></textarea>
                </div>
            </div>
            <div class="row" style="margin-left: 56%">
                <div class="file" style="margin-left: 30px">
                    <input type="submit" name="envoyer" id="file">
                    <label for="file">Envoyer</label>
                </div>

            </div>
        </form>

    </div>


    <!-- ajouter un avi avec Modal-->
    <div class="modal fade bd-example-modal-lg" id="Add_avi" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true" style="margin-top: 10%">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Votre commantaire</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <!-- To display error messages detect  -->
                    <form action="index.php?controller=avi&action=create&produit=<?php echo $u->getId(); ?>"
                          enctype="multipart/form-data" method="post">

                        <div class="form-row" style="margin-top: 8px">
                            <div class="form-group col-md-6">
                                <input type="email" name="email" class="form-control" id="inputEmail4"
                                       placeholder="Votre Email"
                                       required>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">@</div>
                                    </div>
                                    <input type="text" name="pseudo" class="form-control" id="inlineFormInputGroup"
                                           placeholder="Votre pseudo" required>
                                </div>
                            </div>
                        </div>


                        <div class="form-row" style="margin-top: 10px">
                            <div class="form-group col-md-4">
                                <select class="form-control form-control " name="note" required>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>

                                </select>
                            </div>
                            <div class="form-group col-md-8">
                                <input type="file" class="custom-file-input" name="photo" id="validatedCustomFile">
                                <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                            </div>


                        </div>
                        <div class="form-row">
                            <textarea class="form-control" name="commentaire" id="mytextarea"
                                      placeholder="Donner votre avi" rows="3"></textarea>

                        </div>


                        <div class="form-row">
                            <button type="submit" name="envoyer" class="btn btn-primary"
                                    style="margin-top: 2%; margin-left: 80%">Envoyer
                            </button>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>








