<?php
if (isset($_SESSION['Not_exist'])) {
echo '<center style="margin-top: 2px"><div class=" col-7 alert alert-danger" role="alert"><strong> ' . $_SESSION['Not_exist'] . '</strong> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div></center>';
unset($_SESSION['Not_exist']);
}


?>
<section style="margin-top: 2% ; margin-left: 20%">
    <h2>List des produits</h2>
    <div class="container">
        <div class="row">

            <?php
            $i = 1;
            foreach ($tab_u as $u) {
                $u->getQuantite();
                $u->getPrix();

                ?>
                <div class="card mb-3" style=" margin-right: 1%">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="<?= ROOT_IMG ?>produit/defaultimg.png" class="card-img" >
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <span><i class="fa fa-product-hunt" aria-hidden="true"
                                         style="color: darkcyan"></i></span> <span class="card-title"
                                                                                   style="font-size: large; color: teal"> <strong> <a href='index.php?controller=produit&action=read&id=<?php echo $u->getId(); ?>'  ><?= ucfirst($u->getLabel()); ?></a></strong></span>
                                </br>
                                <span> <strong> Prix : <?= $u->getPrix(); ?></strong> <i class="fa fa-eur"
                                                                                         aria-hidden="true"></i></span>  </br>
                                <span> <strong> Quantite : <?= $u->getQuantite(); ?></strong> </span>
                                <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->

                            </div>

                        </div>
                    </div>
                </div>

                <?php


            } ?>
        </div>
    </div>

</section>