<!DOCTYPE html>
<html>
<head>
    <?php
    require_once($ROOT . $DS . "view" . $DS . "head.php"); ?>

    <!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> -->
    <meta charset="utf-8">
    <?php header("Content-Type: text/html; charset=utf-8"); ?>
    <title> <?php echo $pagetitle; ?> </title></head>
</head>
<body>

<?php
require_once($ROOT . $DS . "view" . $DS . "header.php");

$filepath = $ROOT . $DS . "view" . $DS . $controller . $DS;
$filename = "view" . ucfirst($view) . ucfirst($controller) . '.php';
//var_dump($filepath . $filename);
require_once($filepath . $filename);
?>

</body>
<footer>
    <?php
    require_once($ROOT . $DS . "view" . $DS . "footer.php");
    ?>
</footer>
</html>