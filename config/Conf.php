<?php

class Conf
{

    static private $debug = True;
    static private $detabases = array('hostname' => 'localhost', 'database' => 'bdproduitavis', 'login' => 'root', 'password' => '');

    static public function getDebug()
    {
        return self::$debug;
    }

    static public function getHostname()
    {
        return self::$detabases['hostname'];
    }

    static public function getDatabase()
    {
        return self::$detabases['database'];
    }

    static public function getLogin()
    {
        return self::$detabases['login'];
    }

    static public function getPassword()
    {
        return self::$detabases['password'];
    }

}

?>
