<?php
require_once("{$ROOT}{$DS}config{$DS}Conf.php");

class Model
{
    protected static $pdo;

    public static function Init()
    {
        $host = Conf::getHostname();
        $dbname = Conf::getDatabase();
        $login = Conf::getLogin();
        $pass = Conf::getPassword();
        try {
            self::$pdo = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $login, $pass);
        } catch (PDOException $e) {
            die ($e->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public static function getAll()
    {
        $SQL = "SELECT * FROM " . static::$table;
        $rep = self::$pdo->query($SQL);
        $rep->setFetchMode(PDO::FETCH_CLASS,
            'Model' . ucfirst(static::$table));
        return $rep->fetchAll();
    }

 
    /**
     * @param $cle_primaire
     * @return |null
     */
    public static function select($cle_primaire)
    {
        $sql = "SELECT * from " . static::$table . " WHERE
" . static::$primary . "=:cle_primaire";
        $req_prep = self::$pdo->prepare($sql);
        $req_prep->bindParam(":cle_primaire", $cle_primaire);
        $req_prep->execute();
        $req_prep->setFetchMode(PDO::FETCH_CLASS,
            'Model' . ucfirst(static::$table));
        if ($req_prep->rowCount() == 0) {
            return null;
        } else {
            $rslt = $req_prep->fetch();
            return $rslt;
        }
    }

    /**
     * @param $cle_primaire
     */
    function delete($cle_primaire)
    {

        $sql = "DELETE FROM " . static::$table . " WHERE
" . static::$primary . "=:cle_primaire";
        $req_prep = self::$pdo->prepare($sql);
        $req_prep->bindParam(":cle_primaire", $cle_primaire);
        $req_prep->execute();
    }

    /**
     * @param $tab
     * @param $cle_primaire
     */
    public function update($tab, $cle_primaire)
    {
        if (static::$table == 'produit') {
            $label = $tab['label'];
            $prix = $tab['prix'];
            $quantite = $tab['quantite'];
            $description = $tab['description'];
            $sql = "UPDATE `produit` SET `label`='$label',`prix`='$prix',`quantite`='$quantite',`description`='$description';";
            $req_prep = self::$pdo->prepare($sql);
            $req_prep->execute();
            //return $cle_primaire;

        }
    }

    /**
     * @param $tab
     * @return mixed
     */
    public function insert($tab)
    {
        if (static::$table == 'produit') {
            $sql = "INSERT INTO produit (label, prix, quantite, description)
	        VALUE ('" . $tab['label'] . "','" . $tab['prix'] . "','" . $tab['quantite'] . "','" . $tab['description'] . "')";
            $req_prep = self::$pdo->prepare($sql);
            $req_prep->execute();
            return self::$pdo->lastInsertId();
        }elseif (static::$table == 'avi') {
            $sql = "INSERT INTO avi (email, pseudo, note, commentaire, created_at, produit)
	        VALUE ('" . $tab['email'] . "','" . $tab['pseudo'] . "','" . $tab['note'] . "','" . $tab['commentaire'] . "', CURRENT_TIMESTAMP ,'" . $tab['produit'] . "')";
            $req_prep = self::$pdo->prepare($sql);
            $req_prep->execute();
            return self::$pdo->lastInsertId();

        }elseif (static::$table == 'photo') {
            $sql = "INSERT INTO photo (name, extension, avis)
	        VALUE ('" . $tab['name'] . "','" . $tab['ext'] . "','" . $tab['avi'] . "')";
            $req_prep = self::$pdo->prepare($sql);
            $req_prep->execute();
            return self::$pdo->lastInsertId();}
    }


}

Model::Init();