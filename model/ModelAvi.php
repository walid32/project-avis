<?php

require_once("Model.php");
class ModelAvi extends Model
{


    private $id;
    private $email;
    private $pseudo;
    private $note;
    private $commentaire;
    private $created_at;
    private $produit;

    protected static $table = 'avi';
    protected static $primary = 'id';
    public function __construct($id = NULL, $email = NULL, $pseudo =Null, $note = NULL, $commentaire = NULL, $created_at = NULL, $produit = NULL)
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param mixed $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param mixed $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @param mixed $produit
     */
    public function setProduit($produit)
    {
        $this->produit = $produit;
    }


    public static function getAllByProduitDateAsc($produit)
    {

        $SQL = "SELECT * FROM " . static::$table . " WHERE `produit` = '$produit' ORDER BY `created_at` ASC;";
        $rep = self::$pdo->query($SQL);
        $rep->setFetchMode(PDO::FETCH_CLASS,
            'Model' . ucfirst(static::$table));
        return $rep->fetchAll();
    }

    public static function getAllByProduitDateDesc($produit)
    {

        $SQL = "SELECT * FROM " . static::$table . " WHERE `produit` = '$produit' ORDER BY `created_at` DESC;";
        $rep = self::$pdo->query($SQL);
        $rep->setFetchMode(PDO::FETCH_CLASS,
            'Model' . ucfirst(static::$table));
        return $rep->fetchAll();
    }


    public static function getAllByProduitNoteAsc($produit)
    {

        $SQL = "SELECT * FROM " . static::$table . " WHERE `produit` = '$produit' ORDER BY `note` ASC;";
        $rep = self::$pdo->query($SQL);
        $rep->setFetchMode(PDO::FETCH_CLASS,
            'Model' . ucfirst(static::$table));
        return $rep->fetchAll();
    }

    public static function getAllByProduitNoteDesc($produit)
    {

        $SQL = "SELECT * FROM " . static::$table . " WHERE `produit` = '$produit' ORDER BY `note` DESC;";
        $rep = self::$pdo->query($SQL);
        $rep->setFetchMode(PDO::FETCH_CLASS,
            'Model' . ucfirst(static::$table));
        return $rep->fetchAll();
    }


    public static function getAllByProduitAndNote($produit , $Note)
    {

        $SQL = "SELECT * FROM " . static::$table . " WHERE `produit` = '$produit' AND `note` ='$Note';";
        $rep = self::$pdo->query($SQL);
        $rep->setFetchMode(PDO::FETCH_CLASS,
            'Model' . ucfirst(static::$table));
        return $rep->fetchAll();
    }
}