<?php

require_once("Model.php");
class ModelPhoto extends Model
{

    private $id;
    private $name;
    private $extension;
    private $avi;
    protected static $table = 'photo';
    protected static $primary = 'id';
    public function __construct($id = NULL, $name = NULL, $extension = NULL, $avi = NULL)
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param mixed $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return mixed
     */
    public function getAvi()
    {
        return $this->avi;
    }

    /**
     * @param mixed $avi
     */
    public function setAvi($avi)
    {
        $this->avi = $avi;
    }




    /**
     * @param $avi
     * @return |null
     */
    public static function selectPhotoByAvi($avi)
    {
        $sql = "SELECT * FROM `photo` WHERE `avis` = '$avi';";

        $req_prep = self::$pdo->prepare($sql);
        $req_prep->execute();
        $req_prep->setFetchMode(PDO::FETCH_CLASS,
            'Model' . ucfirst(static::$table));
        if ($req_prep->rowCount() == 0) {
            return null;
        } else {
            $rslt = $req_prep->fetch();
            return $rslt;
        }
    }













}