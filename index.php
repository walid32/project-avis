<?php
ob_start();

session_start();
$ROOT = __DIR__;
$DS = DIRECTORY_SEPARATOR;

$x = '\'';
$m = str_replace($DS,'/',$ROOT);


$m = explode($DS,$ROOT);
$racine = $m[count($m)-1];
define('RACINE',$racine );
define('ROOT_IMG','/'.$racine.'/public/img/' );



$controleur_default = "produit";
if (!isset($_REQUEST['controller']))
//$controller récupère $controller_default;
    $controller = $controleur_default;
else
// $controller recupère le contrôleur passé dans l'URL
    $controller = $_REQUEST['controller'];
switch ($controller) {
    case "produit" :
        require("{$ROOT}{$DS}controller{$DS}controllerProduit.php");
        break;
    case "avi" :
        require("{$ROOT}{$DS}controller{$DS}controllerAvi.php");
        break;
    default:
        header('Location: index.php?controller=produit');
        break;
}