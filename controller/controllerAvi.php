<?php



    require_once("{$ROOT}{$DS}model{$DS}ModelAvi.php");
    require_once("{$ROOT}{$DS}model{$DS}ModelPhoto.php");


    if (isset($_REQUEST['action']) )
        /* recupère l'action passée dans l'URL*/
        $action = $_REQUEST['action'];
    /* NB: On a ajouté un comportement par défaut avec action=readAll.*/
    else $action = "readAll";
    switch ($action) {
        case "readAll":
            header('Location: index.php');
            break;
        case "delete":
            if (isset($_REQUEST['id'])) {
                $id = $_REQUEST['id'];
                $produit = $_REQUEST['produit'];
                $del = ModelAvi::select($id);

                if ($del != null) {

                    $img = ModelPhoto::selectPhotoByAvi($id);
                    if($img != null){
                        // delete an image
                        $fileDestination = '..'.ROOT_IMG.'avi/'.$img->getName().'.'.$img->getExtension();
                        unlink($fileDestination);

                    }
                    $del->delete($id);

                    /*redirection vers le contrôleur et l’action par  défaut*/
                        $_SESSION['delete_avis'] = "Votre commentaire est bien supprimé";
                    header('Location: index.php?controller=produit&action=read&id='.$produit);
                }
            }
            break;
        case "create":
            if (isset($_REQUEST['envoyer'])) {
                // if image uploded
                $image = $_FILES['photo'];
                //$_SESSION['testImage'] = $image;
                $produit = $_REQUEST['produit'];
                if($_FILES['photo']['error'] == 0){

                    // traitement d'image

                    $dType = explode('/',$_FILES['photo']['type']);
                    $type = $dType[0];
                    $p = explode('.',$_FILES['photo']['name']);
                    $ext = strtolower(end($p));
                    $allowed = array('jpg','jpeg', 'png');
                    if($dType[0] == 'image' and in_array($ext, $allowed) ){

                        $email  = $_POST["email"];
                        $pseudo = $_POST["pseudo"];
                        $note = $_POST["note"];
                        $commentaire = $_POST["commentaire"];

                        $u = new ModelAvi(); 
                        $tab = array(
                            "email" => $email ,
                            "pseudo" => $pseudo ,
                            "note" => $note ,
                            "commentaire" => $commentaire ,
                            "produit"  => $produit , 
                        );
                        $avi = $u->insert($tab);
                        $nameimg = $produit.'_'.uniqid('CE',true).'_'.$avi;
                        $tabimg = array(
                            "name" => $nameimg ,
                            "ext" => $ext ,
                            "avi" => $avi ,
                        );

                        $c = new ModelPhoto($nameimg , $ext , $avi);
                        $c->insert($tabimg);

                        $fileDestination = '..'.ROOT_IMG.'avi/'.$nameimg.'.'.$ext;
                        move_uploaded_file($_FILES['photo']['tmp_name'], $fileDestination);
                        $_SESSION['AV_create'] = 'Merci pour votre commentaire .';
                        header('Location: index.php?controller=produit&action=read&id='.$produit);
                    }else{


                        $_SESSION['PB_CE_Image'] = TRUE;

                        header('Location: index.php?controller=produit&action=read&id='.$produit);
                    }



                    // Fin traitement d'image

                }else{
                    $email  = $_POST["email"];
                    $pseudo = $_POST["pseudo"];
                    $note = $_POST["note"];
                    $commentaire = $_POST["commentaire"];
                    $produit = $_REQUEST['produit'];
                    $u = new ModelAvi($email , $pseudo , $note , $commentaire , $produit  );
                    $u->setEmail($email);
                    $u->setPseudo($pseudo);
                    $u->setNote($note);
                    $u->setCommentaire($commentaire);
                    $u->setProduit($produit);
                    $tab = array(
                        "email" => $email ,
                        "pseudo" => $pseudo ,
                        "note" => $note ,
                        "commentaire" => $commentaire ,
                        "produit"  => $produit ,
                    );
                    $u->insert($tab);
                    $_SESSION['AV_create'] = 'Merci pour votre commentaire .';
                    header('Location: index.php?controller=produit&action=read&id='.$produit);
                }

            } else {
                $_SESSION['CE_vide'] = TRUE;

                header('Location: index.php?controller=produit&action=read&id='.$_REQUEST['produit']);
            }
            break;
        default:
            header('Location: index.php?controller=produit');

    }
