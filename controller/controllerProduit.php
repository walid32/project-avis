<?php


require_once("{$ROOT}{$DS}model{$DS}ModelProduit.php");
require_once("{$ROOT}{$DS}model{$DS}ModelAvi.php");

if (isset($_REQUEST['action']))
    /* recupère l'action passée dans l'URL*/
    $action = $_REQUEST['action'];
/* NB: On a ajouté un comportement par défaut avec action=readAll.*/
else $action = "readAll";
switch ($action) {
    case "readAll":
        $pagetitle = "List of Produit ";
        $view = "readAll";
        $tab_u = ModelProduit::getAll();
        if (count($tab_u) > 0) {
            //"redirige" vers la vue
            require("{$ROOT}{$DS}view{$DS}view.php");
        } else {
            $pagetitle = "Create une Produit";
            $view = "create";
            $_SESSION['should'] = 'Il faut créer un produit';
            require("{$ROOT}{$DS}view{$DS}view.php");
        }
        break;
    case "read":
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $u = ModelProduit::select($id);   //$listAvis = ModelAvi::getAllByProduit($id);
            if ($u != null) {
                $pagetitle = "Fiche produit";
                $view = "read";
                /* pour trier */


                if (isset($_REQUEST['trier'])) {
                    $att = $_REQUEST['trier'];
                }elseif (isset($_REQUEST['note'])){
                    $att = 'filtrer';
                    $note =  $_REQUEST['note'];
                } else {
                    $att = 'date_asc';
                }


                switch ($att) {
                    case "date_asc":
                        $listAvis = ModelAvi::getAllByProduitDateAsc($id);
                        break;
                    case "date_desc":
                        $listAvis = ModelAvi::getAllByProduitDateDesc($id);
                        break;
                    case "note_asc":
                        $listAvis = ModelAvi::getAllByProduitNoteAsc($id);
                        break;
                    case "note_desc":
                        $listAvis = ModelAvi::getAllByProduitNoteDesc($id);
                        break;
                    case "filtrer":
                        $listAvis = ModelAvi::getAllByProduitAndNote($id,$note );
                        break;
                }
                require("{$ROOT}{$DS}view{$DS}view.php");
            }else{
                $_SESSION['Not_exist'] = 'Désolé, le produit n\'existe pas .';
                header('Location: index.php?controller=produit');
            }
        }
        break;


    default:
        header('Location: index.php?controller=produit');
}
